var path = require('path');

// js压缩
// var webpack = require('webpack');
// var UglifyJsPlugin = webpack.optimize.UglifyJsPlugin;

// html模板插件
var HtmlWebpackPlugin = require('html-webpack-plugin');
// 清理 ./dist 文件夹
var CleanWebpackPlugin = require('clean-webpack-plugin');
// 复制图片
var CopyWebpackPlugin = require('copy-webpack-plugin');
// 单独分离css
// webpack4以上版本不支持啊
// var ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
var MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, './dist')
  },
  module: {
    rules: [{
      test: /\.css$/,
      use: [
          MiniCssExtractPlugin.loader,
          "css-loader"
      ]
    }]
  },
  devServer: {
      contentBase: './dist'
  },
  plugins: [
    // new UglifyJsPlugin(),
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      filename: 'index.html'
    }),
    new CopyWebpackPlugin([
      {
        from: './src/images',
        to: 'images'
      }
    ]),
    new MiniCssExtractPlugin({
      filename: "style.css"
    })
  ],
  mode: 'production'
}
